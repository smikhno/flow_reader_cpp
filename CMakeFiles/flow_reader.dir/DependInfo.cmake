# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/sergey/code/cpp/flow_reader_cpp/lz4.c" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/lz4.c.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/lz4frame.c" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/lz4frame.c.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/lz4hc.c" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/lz4hc.c.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/xxhash.c" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/xxhash.c.o"
  )
set(CMAKE_C_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BOOST_ALL_NO_LIB"
  "BOOST_PROGRAM_OPTIONS_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv4"
  "/usr/local/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/sergey/code/cpp/flow_reader_cpp/Dbscan.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/Dbscan.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/Enclosure.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/Enclosure.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/ImageNode.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/ImageNode.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/LineSpacing.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/LineSpacing.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/PageSegmenter.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/PageSegmenter.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/Reflow.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/Reflow.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/Xycut.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/Xycut.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/common.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/common.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/flow.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/flow.cpp.o"
  "/Users/sergey/code/cpp/flow_reader_cpp/main.cpp" "/Users/sergey/code/cpp/flow_reader_cpp/CMakeFiles/flow_reader.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_PROGRAM_OPTIONS_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv4"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
