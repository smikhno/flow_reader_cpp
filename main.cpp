#include <iostream>
#include "flow.h"
#include <opencv2/opencv.hpp>
#include <string>
#include "Xycut.h"
#include "Reflow.h"
#include "PageSegmenter.h"
#include "common.h"

void reflow(cv::Mat& cvMat, cv::Mat& new_image, float scale, std::vector<glyph> pic_glyphs, cv::Mat& rotated_with_pictures, bool debug) {
    const cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 1));
    
    //cv::morphologyEx(cvMat, cvMat, cv::MORPH_CLOSE, kernel);
    
    
    //cv::dilate(cvMat, cvMat, kernel, cv::Point(-1, -1), 1);
    std::vector<glyph> glyphs = get_glyphs(cvMat);
    
    std::vector<glyph> new_glyphs;
    
    for (glyph g : pic_glyphs) {
        int y = g.y;
        auto it = std::find_if(glyphs.begin(), glyphs.end(), [y] (const glyph& gl) {return gl.y > y;} );
        glyphs.insert(it, g);
    }
    

    
    if (debug) {
        for (int i=0;i<glyphs.size(); i++){
               glyph g = glyphs.at(i);
               cv::rectangle(cvMat, cv::Point(g.x, g.y), cv::Point(g.x + g.width, g.y + g.height), cv::Scalar(255), 5);
           }
    }
    
    
    if (!glyphs.empty()) {
        
        try {
            if (debug) {
                cv::bitwise_not(cvMat, cvMat);
                new_image = cvMat;
            } else {
                Reflow reflower(cvMat, rotated_with_pictures,  glyphs);
                new_image = reflower.reflow(scale, 1.0f);
            }
           
          
        }
        
        catch (...) {
            cv::bitwise_not(rotated_with_pictures, rotated_with_pictures);
            new_image = rotated_with_pictures;
        }
        
        //Reflow reflower(cvMat, rotated_with_pictures,  glyphs);
        //new_image = reflower.reflow(scale);
        //cv::bitwise_not(cvMat, cvMat);
        //new_image = cvMat;
        std::cout << "glyphs count " << glyphs.size() << std::endl;
        
    } else {
        cv::bitwise_not(cvMat, cvMat);
        new_image = cvMat;
    }
   
}



int main(int argc, char* argv[]) {

    if (argc != 4) {
      std::cout << "Usage: flow_reader filename debug scale" << std::endl;
    }



    //Page p = Page("littlewood.png");
    //std::cout << p.height << " " << p.width << std::endl;
    const char* filename = argv[1]; //"dvurog175.png";
    bool debug = (bool)atoi(argv[2]);
    float scale = atof(argv[3]);

    //const char* filename = "dvurog389.png";
    //const char* filename = "k15.png";
    //const char* filename = "k6.png";
    cv::Mat cvMat = cv::imread(filename, 0);

    cv::threshold(cvMat, cvMat, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
    cv::Mat rotated_with_pictures;
    std::vector<glyph> pic_glyphs = preprocess(cvMat, rotated_with_pictures);
    cv::Mat new_image;
        //remove_skew(cvMat);
    //float scale = 2.5f; 
    reflow(cvMat, new_image, scale, pic_glyphs, rotated_with_pictures, debug);
    cv::imwrite("out.png", new_image);
    return 0;
}
